<?php
echo 'inicio <br>';
$mysqli = new mysqli('localhost', 'alumno', 'alumno', 'dweb');
$fallo=true;
/* check connection */
if (mysqli_connect_error()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}
else {echo 'conectado <br>';};



/* set autocommit to off */
$mysqli->autocommit(FALSE);

/* Insert some values */
echo 'insertar <br>';

$mysqli->query("INSERT INTO alumno(numero, nombre, apellidos, edad, fechaMatricula)  VALUES (102, 'Pepe', 'Cuesta López', 43, '2015/9/1')");
if($mysqli->errno) {$fallo=true; $msg = $mysqli->error . '<br>';}
$mysqli->query("INSERT INTO alumno(numero, nombre, apellidos, edad, fechaMatricula)  VALUES (103, 'Pepe', 'Jimenez Contador', 23, '2015/8/30')");
if($mysqli->errno) {$fallo=true; $msg .=$mysqli->error . '<br>';}

/* commit transaction */
if ($fallo){
    $mysqli->rollback();
    echo 'ERROR: <br>' . $msg;
    
}
elseif(!$mysqli->commit()) {
    print("Transaccion fallada\n");
    exit();
}
else{echo 'Transaccion ok';}


/* close connection */
$mysqli->close();
<?php

require_once '/modelo/AlumnoModelo.php';

class Alumno
{

    private $_numero;
    private $_nombre;
    private $_apellidos;
    private $_edad;
    private $_fechaMatricula;
    private $_fechaPruebas;

    public static function getAll()
    {
        $alumnoModelo = new AlumnoModelo;
        $alumnos = $alumnoModelo->getAll();
        return $alumnos;
    }

    function getFechaPruebas()
    {
        return $this->_fechaPruebas;
    }

    public function __construct($numero = NULL, $nombre = "", $apellidos = "", $edad = 0, $fechaMatricula = '2015-09-01')
    {
        $this->_numero = $numero;
        $this->_nombre = $nombre;
        $this->_apellidos = $apellidos;
        $this->_edad = $edad;
        $this->_fechaMatricula = $fechaMatricula;
    }
    
    public function __toString()
    {
        return $this->getNombre() . ' ' . $this->getApellidos();
    }

    function getFechaMatricula()
    {
        return $this->_fechaMatricula;
    }

    function setFechaMatricula($fechaMatricula)
    {
        $this->_fechaMatricula = $fechaMatricula;
    }

    public function getAlumno($numero)
    {
        $alumnoModelo = new AlumnoModelo;
        $fila = $alumnoModelo->getAlumno($numero);
        $this->_numero = $fila['numero'];
        $this->_nombre = $fila['nombre'];
        $this->_apellidos = $fila['apellidos'];
        $this->_edad = $fila['edad'];
        $this->_fechaMatricula = $fila['fechaMatricula'];
        $this->_fechaPruebas = $fila['fechaPruebas'];
    }

    function getNumero()
    {
        return $this->_numero;
    }

    function getNombre()
    {
        return $this->_nombre;
    }

    function getApellidos()
    {
        return $this->_apellidos;
    }

    function getEdad()
    {
        return $this->_edad;
    }

    public function modificar()
    {
        $alumnoModelo = new AlumnoModelo;
        return $alumnoModelo->grabar(get_object_vars($this));
    }

    function setNumero($numero)
    {
        $this->_numero = $numero;
    }

    function setNombre($nombre)
    {
        $this->_nombre = $nombre;
    }

    function setApellidos($apellidos)
    {
        $this->_apellidos = $apellidos;
    }

    function setEdad($edad)
    {
        $this->_edad = $edad;
    }

    public function insertar()
    {
        $alumnoModelo = new AlumnoModelo;
        return $alumnoModelo->insertar($this->_nombre, $this->_apellidos, $this->_edad, $this->_fechaMatricula);
    }

    public function borrar()
    {
        $alumnoModelo = new AlumnoModelo;
        return $alumnoModelo->borrar($this->_numero);
    }

}

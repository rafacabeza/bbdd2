<!DOCTYPE html>
<html>
    <head>
        <title>{$titulo}: {$alumno}</title>
        <style type="text/css">
            a {
                text-align:center;
                font-family: Arial, Helvetica, sans-serif;
                font-size: 14px;
                text-decoration: none;
                color: #EE0000;
            }	
        </style>
    </head>

    <body>

        <h5>Usuario: {$usuario}</h5>
        <h1>{$titulo}: {$alumno}</h1>
        Número: {$alumno->getNumero()}<br>
        Nombre: {$alumno->getNombre()}<br>
        Apellidos: {$alumno->getApellidos()|upper}<br>
        Edad: {$alumno->getEdad()}<br>
        Fecha Matrícula: {$alumno->getFechaMatricula()}<br>
        <p><a href="?modulo=listado">Listado de alumnos</p>
    </body>
</html>
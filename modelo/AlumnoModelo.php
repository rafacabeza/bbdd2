<?php
class AlumnoModelo {

    const USER = 'alumno';
    const BBDD = 'dweb';
    const PSS = 'alumno';
    const HOST = 'localhost';

    private $_db;

    public function __construct() {
        $this->_db = new mysqli(self::HOST, self::USER, self::PSS, self::BBDD);
        if ($this->_db->connect_errno > 0) {
            die("Imposible conectarse con la base de datos [" . $this->_db->connect_error . "]");
        }
    }

    public function getAll() {
        $sql = 'SELECT * FROM alumno';
        $resultado = $this->_db->query($sql);
        return $resultado->fetch_all(MYSQLI_ASSOC);
    }

    public function getAlumno($numero) {
        $sql = 'SELECT *, UNIX_TIMESTAMP(fechaMatricula) as fechaPruebas FROM alumno'
                . ' WHERE numero=' . $numero;
        $resultado = $this->_db->query($sql);
        return $resultado->fetch_assoc();
    }

    public function grabar($fila) {
        $numero = $fila['_numero'];
        $nombre = $fila['_nombre'];
        $apellidos = $fila['_apellidos'];
        $edad = $fila['_edad'];
        $fechaMatricula = $fila['_fechaMatricula'];

        $sql = "UPDATE alumno"
                . " SET nombre = '$nombre',"
                . " apellidos = '$apellidos',"
                . " edad = $edad, "
                . " fechaMatricula = '$fechaMatricula' "
                . " WHERE numero = $numero";

        var_dump($sql);
        $resultado = $this->_db->query($sql);
        return $resultado;
    }

    public function insertar($nombre, $apellidos, $edad, $fechaMatricula) {
        $sql = "INSERT INTO alumno(nombre, apellidos, edad, fechaMatricula) "
                . " VALUES ('$nombre',"
                . " '$apellidos',"
                . " $edad, "
                . " '$fechaMatricula')";
        
        if($this->_db->errno!=0){
            die('Mofificación fallida. <br>' . $this->_db->err);
        }
        $resultado = $this->_db->query($sql);
        return $resultado;
    }
    
    public function borrar($numero)
    {
        $sql="DELETE FROM alumno "                
                . " WHERE numero= $numero";
        $resultado = $this->_db->query($sql);
        return $resultado;
    }

}

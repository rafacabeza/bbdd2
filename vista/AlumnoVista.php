<?php
require_once('../smarty/libs/Smarty.class.php');

abstract class AlumnoVista {
    private static function _iniciarVista(&$vista)
    {
            $vista = new Smarty();
            $vista->template_dir = 'plantilla';
            $vista->compile_dir = 'tmp';
    }

    public static function mostrarListado($alumnos, $mensaje="")
    {
        $vista;
        self::_iniciarVista($vista);
        
        $vista->assign('titulo', 'Listado de Alumnos');
        $vista->assign('usuario', $_SESSION['usuario']);
        $vista->assign('mensaje', $mensaje);
        $vista->assign('alumnos', $alumnos);
//        var_dump($alumnos);
        $vista->display('listado.tpl');              
    }
    
    public static function mostrarDetalle(Alumno $alumno)
    {
        self::_iniciarVista($vista);        
        $vista->assign('titulo', 'Datos del alumno');
        $vista->assign('usuario', $_SESSION['usuario']);
        $vista->assign('alumno', $alumno);        
        $vista->display('alumnoDetalle.tpl');
    }
    
    public static function mostrarFormInsertar()
    {
        self::_Formulario();                
    }
    
    public static function mostrarFormModificar(Alumno $alumno) {   
        $html = self::_Formulario(
        $alumno->getNumero(),
        $alumno->getNombre(),
        $alumno->getApellidos(),
        $alumno->getEdad(),
        $alumno->getFechaMatricula(),
        'modificar');
    }

    static private function _Formulario($numero = "", $nombre = "", 
            $apellidos = "", $edad="", $fechaMatricula = "" , $accion = "insertar")
    {
        self::_iniciarVista($vista);        
        $vista->assign('titulo', 'Modificar alumno');
        $vista->assign('usuario', $_SESSION['usuario']);
        $vista->assign('numero', $numero);        
        $vista->assign('nombre', $nombre);        
        $vista->assign('apellidos', $apellidos);        
        $vista->assign('edad', $edad);        
        $vista->assign('fechaMatricula', $fechaMatricula);        
        $vista->assign('accion', $accion);        
        $vista->display('alumnoForm.tpl');
     }
}

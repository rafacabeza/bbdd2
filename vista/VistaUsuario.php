<?php

class VistaUsuario {

    public static function login($nombre = "", $password = "", $mensaje = "") {
        $template = file_get_contents('plantilla/login.html');
        $template = str_replace('{nombre}', $nombre, $template);
        $template = str_replace('{password}', $password, $template);
        $template = str_replace('{mensaje}', $mensaje, $template);
        echo $template;
    }

    public static function bienvenido($nombre) {
        $template = file_get_contents('bienvenido.html');
        $template = str_replace('{nombre}', $nombre, $template);
        echo $template;
    }

}

<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$mysqli = new mysqli('localhost', 'alumno', 'alumno', 'dweb');
if ($mysqli->connect_errno) die($mysqli->connect_error);




/* check connection */
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}


/* create a prepared statement */
echo '<h1> Forma preparada 1</h1>';
if ($stmt = $mysqli->prepare("SELECT apellidos FROM alumno WHERE nombre=?")) {

    /* bind parameters for markers */
    $nombre = "Pepe";
    $stmt->bind_param("s", $nombre);

    /* execute query */
    $stmt->execute();

    /* bind result variables */
//    $stmt->bind_result($apellidos);
    
    /* instead of bind_result: */
    $result = $stmt->get_result();

    while ($myrow = $result->fetch_assoc()) {

        // use your $myrow array as you would with any other fetch
        printf("%s se apellida %s\n", $nombre, $myrow['apellidos'] . '<br>');

    }
    

}
else{echo 'mal preparada';}


echo '<h1> Forma preparada 2</h1>';

/* execute query */
$stmt->execute();

/* bind result variables */
$stmt->bind_result($apellidos);

while ($stmt->fetch()) {

    // use your $myrow array as you would with any other fetch
    printf("%s se apellida %s\n", $nombre, $apellidos . '<br>');

}


/* close statement */
$stmt->close();



echo '<h1> Forma tradicional</h1>';

$consulta = $mysqli->query("SELECT * FROM alumno WHERE nombre='Pepe'");

while ($fila=$consulta->fetch_assoc()){
    printf("%s se apellida %s\n", $nombre, $fila['apellidos'] . '<br>');
}

/* close connection */
$mysqli->close();